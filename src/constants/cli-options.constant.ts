export const CLI_OPTIONS = {
  append: 'Version 1.x.x',
  options: [
    {
      alias: 'h',
      description: 'Displays help',
      option: 'help',
      type: 'Boolean'
    },
    {
      alias: 'c',
      description: 'Relative path to the configuration file (defaults to ./generator.json)',
      example: 'buildr --config ./generator.json',
      option: 'config',
      type: 'String'
    }
  ],
  prepend: 'Usage: buildr [options]'
};
