import * as fs from 'fs';
import * as path from 'path';
import glob from 'glob';
import * as Handlebars from 'handlebars';
import { isEqual, isObject, transform } from 'lodash';
import * as prettier from 'prettier';
import * as showdown from 'showdown';

/**
 * Recursively creates a directory
 *
 * @param _path The path of the directory tree you want to create
 * @returns A promise that resolves when the operation is complete
 */
export async function createDir(_path: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.mkdir(_path, { recursive: true }, (error) => {
      if (error) {
        reject(`Failed to create directory ${_path}`);
      } else {
        resolve();
      }
    });
  });
}

/**
 * Compares two objects
 *
 * @param base The base object
 * @param obj The object you want to find the missing key/values of
 * @returns An object with the key/values that the obj is missing to be of base type
 */
export function diffObjects(base: {}, obj: {}): {} {
  return transform(obj, (result, value, key) => {
    if (!isEqual(value, base[key])) {
      if (!base[key]) {
        result[key] = isObject(value) && isObject(base[key]) ? diffObjects(value, base[key]) : value; // eslint-disable-line
      }
    }
  });
}

/**
 * Return an array of files in a directory
 *
 * @param _path The path to scan for files
 * @returns A promise that resolves an array of files
 */
export async function dirFiles(_path: string): Promise<readonly string[]> {
  return new Promise((resolve, reject) => {
    fs.readdir(_path, (error, files) => {
      if (error) {
        reject(error);
      } else {
        resolve(files.map((x) => path.resolve(`${_path}/${x}`)));
      }
    });
  });
}

/**
 * Uses [Prettier](https://www.npmjs.com/package/prettier) to properly
 * format strings before saving them to files
 *
 * @param str The string to pass through [prettier](https://www.npmjs.com/package/prettier)
 * @returns A properly formatted string
 */
export function format(str: string, options: any = {}): string {
  return prettier.format(str, options);
}

/**
 * Uses a glob pattern to find matching files
 *
 * @param pattern The pattern to match
 * @returns An array of files that match
 */
export async function globber(pattern: string): Promise<readonly string[]> {
  return new Promise((resolve, reject) => {
    glob(pattern, null as any, (error, files) => {
      if (error) {
        reject(error);
      } else {
        resolve(files);
      }
    });
  });
}

/**
 * Converts a markdown file to HTML
 *
 * @param path
 * @param context
 */
export async function mdFileToHTML(_path: string, context: object = {}): Promise<string> {
  return new Promise((resolve, reject) => {
    try {
      const markdown = fs.readFileSync(_path).toString();
      const template = Handlebars.compile(markdown);
      const text = template(context);
      const converter = new showdown.Converter();
      const html = converter.makeHtml(text);
      resolve(html);
    } catch (e) {
      reject(e);
    }
  });
}

/**
 * Compiles a file with Handlebars and then saves it
 *
 * @param source
 * @param target
 * @param context
 */
export async function saveHandlebars(source: string, target: string, context: object = {}): Promise<void> {
  return new Promise(async (resolve, reject) => {
    try {
      const src = fs.readFileSync(source).toString();
      const template = Handlebars.compile(src);
      const output = template(context);
      await writeFile(target, output);
      resolve();
    } catch (e) {
      reject(e);
    }
  });
}

/**
 * Determines whether the file is valid (whether it exists or not for now)
 *
 * @param _path The path to the file
 * @returns True if the file is valid
 */
export function validFile(_path: string): boolean {
  return fs.existsSync(_path);
}

/**
 * Saves a file
 *
 * @param _path The absolute path to the file
 * @param data The data being written to the file
 * @returns A promise that resolves if the file was written without error
 */
export async function writeFile(_path: string, data: any): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.writeFile(_path, data, (error) => {
      if (error) {
        reject(`Failed to save ${_path}`);
      } else {
        resolve();
      }
    });
  });
}
