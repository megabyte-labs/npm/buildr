import * as fs from 'fs';
import * as path from 'path';
import { Buildr } from './app';
import { CLI_OPTIONS } from './constants/cli-options.constant';
import { Logger } from './lib/log';

const optionator = require('optionator')(CLI_OPTIONS); // eslint-disable-line

/** Delegate CLI commands to appropriate logic */
export async function cli(argz: any): Promise<void> {
  const options = optionator.parseArgv(argz);
  const configPath = options.config ? options.config : './generator.json';
  if (options.help) {
    console.log(optionator.generateHelp()); // eslint-disable-line
  }
  const _path = path.resolve(process.cwd(), configPath); // eslint-disable-line no-underscore-dangle
  if (fs.existsSync(_path)) {
    if (fs.lstatSync(_path).isDirectory()) {
      Logger.error(
        'The path to the configuration file you specified appears to be a directory. A JSON file was expected.'
      );
    } else {
      try {
        const config = await import(_path);
        Buildr.run(config); // Entry point
      } catch (e) {
        Logger.error(
          'Failed to import the specified configuration. Something is probably wrong with the JSON - try validating it.'
        );
      }
    }
  } else {
    Logger.error(`The path to the configuration file you specified (${_path}) does not exist`);
  }
}
