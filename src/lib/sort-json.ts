import * as fs from 'fs';
import jsonabc from 'jsonabc';
import { globber, writeFile } from './util';

/**
 * Sorts the JSON files that are passed in
 *
 * @param patterns
 */
export async function sortJSON(patterns: readonly string[]): Promise<void> {
  const matches = await Promise.all(patterns.map(async (x) => globber(x)));
  const flattened = matches.flat();
  await Promise.all(
    flattened.map(async (x) => {
      const input = fs.readFileSync(x).toString();
      await writeFile(x, jsonabc.sort(input, false).toString());
    })
  );
}
