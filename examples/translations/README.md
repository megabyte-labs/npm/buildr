# JSON Translation Example

This example translates many files by passing two sources to the Buildr configuration. The first source is the `en.json` file and this will be translated to the languages specified in buildr.json. The translations will reside next to the source file. A folder is also specified as a source. Buildr will recursively generate the translations and place them into folders that correspond to their translated languages.

To run this sample, in the current directory run the following commands:

```
npm install -g @woogie/buildr
buildr -c buildr.json
```
