/* eslint-disable @typescript-eslint/naming-convention */
export const ValidationErrorMessage = {
  TranslationSourceOptions: {
    IsString: 'The path must be a string!',
    IsTrue: 'The path must link to a valid file! Does it exist?'
  }
};
