# JSON Sort Example

This configuration will sort a JSON file named `sample.json` alphabetically.

To evaluate this example, in the current directory run the following commands:

```
npm install -g @woogie/buildr
buildr -c buildr.json
```
