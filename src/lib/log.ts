import { default as Pino } from 'pino';

const pinoChildMinLength = 1;

export const pinoLog = Pino({
  prettyPrint: {
    ignore: 'hostname,pid',
    translateTime: 'SYS:HH:MM:ss.l o'
  }
});

/**
 * Shared logger used instead of console.log
 */
export class Logger {
  /**
   * Debug log level
   *
   * @param args
   */
  public static debug(...args): void {
    if (args && args.length > pinoChildMinLength) {
      const child = pinoLog.child({ debug: args[1] });
      child.debug(args[0]);
    } else {
      pinoLog.debug(...args);
    }
  }

  /**
   * Error log level
   *
   * @param args
   */
  public static error(...args): void {
    if (args && args.length > pinoChildMinLength) {
      const child = pinoLog.child({ error: args[1].toString(), data: args[1] });
      child.error(args[0]);
    } else {
      pinoLog.error(...args);
    }
  }

  /**
   * Info log level
   *
   * @param args
   */
  public static info(...args): void {
    if (args && args.length > pinoChildMinLength) {
      const child = pinoLog.child({ info: args[1] });
      child.info(args[0]);
    } else {
      pinoLog.info(...args);
    }
  }

  /**
   * Alias for info log level
   *
   * @param args
   */
  public static log(...args): void {
    Logger.info(...args);
  }

  /**
   * Warning log level
   *
   * @param args
   */
  public static warn(...args): void {
    if (args && args.length > pinoChildMinLength) {
      const child = pinoLog.child({ warning: args[1] });
      child.warn(args[0]);
    } else {
      pinoLog.warn(...args);
    }
  }
}
