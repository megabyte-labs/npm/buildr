import { IsArray, IsOptional, IsString, ValidateNested } from 'class-validator';
import { CloudFlareOptions } from './cloudflare-options.model';
import { HandlebarsOptions } from './handlebars-options.model';
import { JSONMarkdownOptions } from './json-md-options.model';
import { SceneOptions } from './scene-options.model';
import { ScreenshotOptions } from './screenshot-options.model';
import { SharpOptions } from './sharp-options.model';
import { TranslationsOptions } from './translations-options.model';

/**
 * The master data model that includes all of the other models
 */
export class BuildrOptions {
  @IsOptional()
  @ValidateNested()
  public readonly cloudflare?: CloudFlareOptions;
  @IsOptional()
  @ValidateNested()
  public readonly faviconGenerator?: any;
  @IsOptional()
  @IsArray()
  @ValidateNested()
  public readonly handlebars: HandlebarsOptions[] = [];
  @IsOptional()
  @IsArray()
  @ValidateNested()
  public readonly jsonMd: JSONMarkdownOptions[] = [];
  @IsOptional()
  @IsArray()
  @IsString({
    each: true
  })
  public readonly jsonSort: string[] = [];
  @IsOptional()
  @IsArray()
  @ValidateNested()
  public readonly scenes: SceneOptions[] = [];
  @IsOptional()
  @IsArray()
  @ValidateNested()
  public readonly screenshots: ScreenshotOptions[] = [];
  @IsOptional()
  @IsArray()
  @ValidateNested()
  public readonly sharp: SharpOptions[] = [];
  @IsOptional()
  @ValidateNested()
  public readonly translations?: TranslationsOptions;

  public constructor(private readonly options: BuildrOptions) {
    if (this.options.cloudflare) {
      this.cloudflare = new CloudFlareOptions(this.options.cloudflare);
    }
    if (this.options.faviconGenerator) {
      this.faviconGenerator = this.options.faviconGenerator;
    }
    if (this.options.handlebars) {
      for (const handlebars of this.options.handlebars) {
        const option = new HandlebarsOptions(handlebars);
        this.handlebars.push(option);
      }
    }
    if (this.options.jsonMd) {
      for (const json of this.options.jsonMd) {
        const option = new JSONMarkdownOptions(json);
        this.jsonMd.push(option);
      }
    }
    this.jsonSort = this.options.jsonSort;
    if (this.options.scenes) {
      for (const scene of this.options.scenes) {
        const option = new SceneOptions(scene);
        this.scenes.push(option);
      }
    }
    if (this.options.screenshots) {
      for (const screenshot of this.options.screenshots) {
        const option = new ScreenshotOptions(screenshot);
        this.screenshots.push(option);
      }
    }
    if (this.options.sharp) {
      for (const sharp of this.options.sharp) {
        const option = new SharpOptions(sharp);
        this.sharp.push(option);
      }
    }
    if (this.options.translations) {
      this.translations = new TranslationsOptions(this.options.translations);
    }
  }
}
