import { IsArray, IsOptional, IsString, MinLength } from 'class-validator';
import { validFile } from '../lib/util';
import { IsTrue } from '../lib/validators/is-true.validator';

const minLanguageCodeLength = 2;

/**
 * The data model assigned to translations
 */
export class TranslationsOptions {
  @IsOptional()
  @IsArray()
  @MinLength(minLanguageCodeLength, {
    each: true
  })
  @IsString({
    each: true
  })
  /**
   * An optional array of languages to translate to. If you specify this,
   * the script will create new files for languages that do not already have
   * files.
   */
  public readonly languages?: readonly string[];
  @IsString()
  /** The projectId associated with the Google Translate API credentials */
  public readonly projectId: string;
  @IsArray()
  @IsTrue(validFile, {
    each: true
  })
  /**
   * An array of file paths to either an [Two_letter_language_code].json file
   * or a path that ends with a folder named with the two letter language code
   */
  public readonly source: readonly string[];

  public constructor(private readonly options: TranslationsOptions) {
    this.languages = this.options.languages;
    this.projectId = this.options.projectId;
    this.source = this.options.source;
  }
}
