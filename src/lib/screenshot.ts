import * as fs from 'fs';
import puppeteer from 'puppeteer';
import { ScreenshotOptions } from '../models/screenshot-options.model';

const screenshotHttpDelay = 1000;
const screenshotLocalDelay = 0;

/**
 * Checks if screenshots exist and acquires them if they do not
 *
 * @param screenshots An array of [[ScreenshotOptions]]
 * @return A void Promise
 */
export async function getScreenshots(screenshots: readonly ScreenshotOptions[]): Promise<void> {
  await Promise.all(
    screenshots
      .filter((x) => !fs.existsSync(x.output)) // Filter out screenshots that already exist
      .map((x) => ({ url: x.input, output: x.output, viewport: { height: x.height, width: x.width } }))
  );
}

/**
 * Acquires a screenshot using [puppeteer](https://www.npmjs.com/package/puppeteer)
 * and saves it to disk
 *
 * @param url The URL of the desired screenshot - can be either an http:// URL or a relative file path
 * @param output The destination of the screenshot
 * @param viewport An object with both the height and width parameters that specify
 * the viewport size of the screenshot
 * @returns A void promise
 */
export async function screenshot(
  url: string,
  output: string,
  viewport: { readonly height: number; readonly width: number }
): Promise<void> {
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport(viewport);
  const isHttp = url.startsWith('http');
  await page.goto(isHttp ? url : `file:${url}`);
  await new Promise((resolve) => setTimeout(() => resolve, isHttp ? screenshotHttpDelay : screenshotLocalDelay));
  await page.screenshot({ path: output });
  await browser.close();
}
