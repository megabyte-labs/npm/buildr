<!-- ⚠️ This README has been generated from the file(s) "./.modules/docs/blueprint-readme.md" ⚠️--><div align="center">
  <center>
    <a href="https://gitlab.com/megabyte-labs/npm/ansibler" title="@megabytelabs/ansibler GitLab page" target="_blank">
      <img width="100" height="100" alt="@megabytelabs/ansibler logo" src="https://gitlab.com/megabyte-labs/npm/ansibler/-/raw/master/logo.png" />
    </a>
  </center>
</div>
<div align="center">
  <center><h1 align="center">NPM Package: Ansibler</h1></center>
</div>

<div align="center">
  <h4 align="center">
    <a href="https://megabyte.space" title="Megabyte Labs homepage" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/home-solid.svg" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansibler" title="@megabytelabs/ansibler package on npmjs.org" target="_blank">
      <img height="50" src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/npm.svg" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansibler/-/blob/master/CONTRIBUTING.md" title="Learn about contributing" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/contributing-solid.svg" />
    </a>
    <a href="https://www.patreon.com/ProfessorManhattan" title="Support us on Patreon" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/support-solid.svg" />
    </a>
    <a href="https://app.slack.com/client/T01ABCG4NK1/C01NN74H0LW/details/" title="Slack chat room" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/chat-solid.svg" />
    </a>
    <a href="https://github.com/ProfessorManhattan/npm-ansibler" title="GitHub mirror" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/github-solid.svg" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansibler" title="GitLab repository" target="_blank">
      <img src="https://gitlab.com/megabyte-labs/assets/-/raw/master/svg/gitlab-solid.svg" />
    </a>
  </h4>
  <p align="center">
    <a href="https://www.npmjs.com/package/@megabytelabs/ansibler" target="_blank">
      <img alt="Version: 1.0.3" src="https://img.shields.io/badge/version-1.0.3-blue.svg?cacheSeconds=2592000&style=badge_style" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansibler/commits/master" title="GitLab CI build status" target="_blank">
      <img alt="Build status" src="https://gitlab.com/megabyte-labs/npm/ansibler/badges/master/pipeline.svg">
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansibler" title="Dependency status reported by Depfu">
      <img alt="Dependency status reported by Depfu" src="https://img.shields.io/depfu/megabyte-labs/npm-ansibler?style=badge_style&logo=npm" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansibler" title="Zip file size">
      <img alt="Zip file size" src="https://img.shields.io/bundlephobia/minzip/@megabytelabs/ansibler?style=bad_style&logo=npm" />
    </a>
    <a href="" title="Total downloads of @megabytelabs/ansibler on npmjs.org">
      <img alt="Total downloads of @megabytelabs/ansibler on npmjs.org" src="https://img.shields.io/npm/dt/@megabytelabs/ansibler?logo=npm&style=badge_style&logo=npm" />
    </a>
    <a href="https://www.npmjs.com/package/@megabytelabs/ansibler" title="Number of vulnerabilities from Snyk scan on @megabytelabs/ansibler">
      <img alt="Number of vulnerabilities from Snyk scan on @megabytelabs/ansibler" src="https://img.shields.io/snyk/vulnerabilities/npm/@megabytelabs/ansibler?style=badge_style&logo=npm" />
    </a>
    <a href="https://megabyte.space/docs/npm" target="_blank">
      <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg?logo=readthedocs&style=badge_style" />
    </a>
    <a href="https://gitlab.com/megabyte-labs/npm/ansibler/-/raw/master/LICENSE" target="_blank">
      <img alt="License: MIT" src="https://img.shields.io/badge/license-MIT-yellow.svg?style=badge_style" />
    </a>
    <a href="https://opencollective.com/megabytelabs" title="Support us on Open Collective" target="_blank">
      <img alt="Open Collective sponsors" src="https://img.shields.io/opencollective/sponsors/megabytelabs?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAAElBMVEUAAACvzfmFsft4pfD////w+P9tuc5RAAAABHRSTlMAFBERkdVu1AAAAFxJREFUKM9jgAAXIGBAABYXMHBA4yNEXGBAAU2BMz4FIIYTNhtFgRjZPkagFAuyAhGgHAuKAlQBCBtZB4gzQALoDsN0Oobn0L2PEUCoQYgZyOjRQFiJA67IRrEbAJImNwFBySjCAAAAAElFTkSuQmCC&label=Open%20Collective%20sponsors&style=badge_style" />
    </a>
    <a href="https://github.com/ProfessorManhattan" title="Support us on GitHub" target="_blank">
      <img alt="GitHub sponsors" src="https://img.shields.io/github/sponsors/ProfessorManhattan?label=GitHub%20sponsors&logo=github&style=badge_style" />
    </a>
    <a href="https://github.com/ProfessorManhattan" target="_blank">
      <img alt="GitHub: ProfessorManhattan" src="https://img.shields.io/github/followers/ProfessorManhattan?style=social" target="_blank" />
    </a>
    <a href="https://twitter.com/MegabyteLabs" target="_blank">
      <img alt="Twitter: MegabyteLabs" src="https://img.shields.io/twitter/url/https/twitter.com/MegabyteLabs.svg?style=social&label=Follow%20%40MegabyteLabs" />
    </a>
  </p>
</div>

> </br><h3 align="center">**A collection of build tools useful for generating meta files, image compression, i18n, and compiling Handlebars templates**</h3></br>

<!--TERMINALIZER![terminalizer_title](https://gitlab.com/megabyte-labs/npm/role_name/-/raw/master/.demo.gif)TERMINALIZER-->

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#table-of-contents)

## ➤ Table of Contents

- [➤ Requirements](#-requirements)
- [➤ Contributing](#-contributing)
- [➤ License](#-license)

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#requirements)

## ➤ Requirements

- **[Node.js >9](https://gitlab.com/megabyte-labs/ansible-roles/nodejs)**

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#contributing)

## ➤ Contributing

Contributions, issues, and feature requests are welcome! Feel free to check the [issues page](https://gitlab.com/megabyte-labs/npm/ansibler/-/issues). If you would like to contribute, please take a look at the [contributing guide](https://gitlab.com/megabyte-labs/npm/ansibler/-/blob/master/CONTRIBUTING.md).

<details>
<summary>Sponsorship</summary>
<br/>
<blockquote>
<br/>
I create open source projects out of love. Although I have a job, shelter, and as much fast food as I can handle, it would still be pretty cool to be appreciated by the community for something I have spent a lot of time and money on. Please consider sponsoring me! Who knows? Maybe I will be able to quit my job and publish open source full time.
<br/><br/>Sincerely,<br/><br/>

**_Brian Zalewski_**<br/><br/>

</blockquote>

<a href="https://www.patreon.com/ProfessorManhattan">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

</details>

[![-----------------------------------------------------](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/aqua.png)](#license)

## ➤ License

Copyright © 2021 [Megabyte LLC](https://megabyte.space). This project is [MIT](https://gitlab.com/megabyte-labs/npm/ansibler/-/raw/master/LICENSE) licensed.
