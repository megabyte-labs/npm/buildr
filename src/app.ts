import { validate } from 'class-validator';
import { Logger } from './lib/log';
import { getScreenshots } from './lib/screenshot';
import { sortJSON } from './lib/sort-json';
import { TranslateService } from './lib/translate';
import { BuildrOptions } from './models/buildr-options.model';
// import { CloudFlareOptions } from './models/cloudflare-options.model';
// import { HandlebarsOptions } from './models/handlebars-options.model';
// import { SceneOptions } from './models/scene-options.model';
import { ScreenshotOptions } from './models/screenshot-options.model';
// import { SharpOptions } from './models/sharp-options.model';
import { TranslationsOptions } from './models/translations-options.model';

/**
 * This class exposes the base API for Buildr. In general, you should be
 * using this as the entry point.
 *
 * Example usage:
 * ```
 * const config = await import('./generator-config.json');
 * Buildr.run(config);
 * ```
 */
export class Buildr {
  /**
   * This method runs through all the tasks that are provided by Buildr.
   *
   * @param data Options in the form of [[BuildrOptions]]
   * @returns A void Promise
   */
  public static async run(data: BuildrOptions): Promise<void> {
    const config = new BuildrOptions(data);
    const errors = await validate(config);
    if (errors.length) {
      Logger.error(errors);
    } else {
      // BuildrOptions is valid
      if (config.translations) {
        await Buildr.translate(config.translations);
      }
      await Buildr.jsonSort(config.jsonSort);
    }
  }

  /**
   * Generates favicons and other related assets using
   * [gulp-real-favicon](https://www.npmjs.com/package/gulp-real-favicon)
   *
   * @param data The JSON that you can generate on [Favicon Generator's website](https://realfavicongenerator.net/)
   * @returns A void Promise
   */
  /*
  public static async faviconGenerator(data: any): Promise<void> {
    Logger.log(data);
  }
  */

  /**
   * TODO
   *
   * @param data
   */
  /*
  public static async handlebars(data: readonly HandlebarsOptions[]): Promise<void> {
    Logger.log(data);
  }
  */

  /**
   * Given an array of locations, this will re-save the JSON files ordered
   * alphabetically
   *
   * @param data An array of file globs
   * @returns A void Promise
   */
  public static async jsonSort(data: readonly string[]): Promise<void> {
    try {
      if (data.length) {
        await sortJSON(data);
      }
    } catch (e) {
      Logger.error('Failed to run JSON sort', e);
    }
  }
  /**
   * Takes a very-very-very wide image and splits into parts that,
   * when combined with the right CSS, can be added to a slider to make it
   * look like the images are all one image. This may seem simple but when
   * using background-size: cover; and height: 100vh; it may not work properly.
   * That is why it generates different images for different screen sizes.
   *
   * Example of site using this: [https://videoblobs.com](https://videoblobs.com)
   *
   * @param data Options in form of [[SceneOptions]]
   * @returns A void Promise
   */
  /*
  public static async scenes(data: readonly SceneOptions[]): Promise<void> {
    Logger.log(data);
  }
  */

  /**
   * Uses [puppeteer](https://www.npmjs.com/package/puppeteer) to take
   * screenshots and then optionally compresses the images using
   * [tinify](https://www.npmjs.com/package/tinify)
   *
   * @param data Options in the form of [[ScreenshotOptions]]
   * @returns A void Promise
   */
  public static async screenshots(data: readonly ScreenshotOptions[]): Promise<void> {
    try {
      await getScreenshots(data);
    } catch (e) {
      Logger.error('Failed to acquire screenshots', e);
    }
  }

  /**
   * Generates images using [sharp](https://www.npmjs.com/package/sharp)
   *
   * @param data Options in the form of [[SharpOptions]]
   * @returns A void Promise
   */
  /*
  public static async sharp(data: readonly SharpOptions[]): Promise<void> {
    Logger.log(data);
  }
  */

  /**
   * Translates JSON content using [@google-cloud/translate](https://www.npmjs.com/package/@google-cloud/translate)
   *
   * @param data Options in the form of [[TranslationsOptions]]
   * @returns A void Promise
   */
  public static async translate(data: TranslationsOptions): Promise<void> {
    try {
      if (data) {
        Logger.info('Detecting missing translations');
        const translate = new TranslateService(data);
        await translate.run();
      }
    } catch (e) {
      Logger.error('Failed to complete translation sequence', e);
    }
  }

  /**
   * Updates CloudFlare Worker KV storage
   *
   * @param data Options in the form of [[CloudFlareOptions]]
   * @returns A void Promise
   */
  /*
  public static async updateCloudFlareKV(data: CloudFlareOptions): Promise<void> {
    Logger.log(data);
  }
  */
}
