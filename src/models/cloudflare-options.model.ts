import * as dotenv from 'dotenv';
dotenv.config();
import { Logger } from '../lib/log';

/**
 * The data model assigned to CloudFlare jobs
 */
export class CloudFlareOptions {
  public constructor(private readonly options: CloudFlareOptions) {
    Logger.log(this.options);
  }
}
